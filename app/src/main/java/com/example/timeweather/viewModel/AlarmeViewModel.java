package com.example.timeweather.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.timeweather.database.entity.Alarme;
import com.example.timeweather.repository.AlarmeRepository;

import java.util.List;

public class AlarmeViewModel extends AndroidViewModel {

    private AlarmeRepository alarmeRepository;
    private LiveData<List<Alarme>> alarmes;

    public AlarmeViewModel(@NonNull Application application) {
        super(application);
        alarmeRepository = new AlarmeRepository(application);
        alarmes = alarmeRepository.getAllAlarmes();
    }

    public LiveData<List<Alarme>> getAlarmes() {
        return alarmes;
    }

    public void insert(Alarme a){
        alarmeRepository.insertAlarm(a);
    }

    public void delete(Alarme a){
        alarmeRepository.deleteAlarm(a);
    }
}
