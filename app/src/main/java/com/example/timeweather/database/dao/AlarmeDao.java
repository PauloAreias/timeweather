package com.example.timeweather.database.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.timeweather.database.entity.Alarme;

import java.util.List;

@Dao
public interface AlarmeDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Alarme alarme);

    @Delete
    void delete(Alarme alarme);

    @Query("SELECT * FROM alarme ORDER BY id ASC")
    LiveData<List<Alarme>> getAll();


}
