package com.example.timeweather.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.timeweather.database.AlarmeDatabase;
import com.example.timeweather.database.dao.AlarmeDao;
import com.example.timeweather.database.entity.Alarme;

import java.util.List;

public class AlarmeRepository {

    private AlarmeDao alarmeDao;
    private LiveData<List<Alarme>> allAlarmes;

    public AlarmeRepository(Application application){
        AlarmeDatabase db = AlarmeDatabase.getInstance(application);
        alarmeDao = db.alarmeDao();
        allAlarmes = alarmeDao.getAll();
    }

    public LiveData<List<Alarme>> getAllAlarmes() {
        return allAlarmes;
    }

    public void insertAlarm(Alarme alarme){
        new InsertAsync().execute(alarme);
    }

    public void deleteAlarm(Alarme alarme){
        new DeleteAsync(alarmeDao).execute(alarme);
    }


    private class InsertAsync extends AsyncTask<Alarme,Void,Void>{

        @Override
        protected Void doInBackground(Alarme... alarmes) {
            alarmeDao.insert(alarmes[0]);
            return null;
        }
    }

    private class DeleteAsync extends AsyncTask<Alarme,Void,Void>{

        private AlarmeDao alarmeDao;

        DeleteAsync(AlarmeDao dao){
            alarmeDao = dao;
        }

        @Override
        protected Void doInBackground(Alarme... alarmes) {
            alarmeDao.delete(alarmes[0]);
            return null;
        }
    }
}
