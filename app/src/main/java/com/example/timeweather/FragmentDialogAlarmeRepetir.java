package com.example.timeweather;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class FragmentDialogAlarmeRepetir extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final String [] lista_repetir = getActivity().getResources().getStringArray(R.array.lista_repetir);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Selecione uma opção:");
        builder.setNeutralButton("Cancelar", null);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "FUNCIONOU", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setMultiChoiceItems(lista_repetir, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i, boolean isChecked) {
                if(isChecked){

                    Toast.makeText(getActivity(), "Escolheu: "+lista_repetir[i], Toast.LENGTH_SHORT).show();
                }else{

                    Toast.makeText(getActivity(), "Retirou: "+lista_repetir[i], Toast.LENGTH_SHORT).show();
                }
            }
        });

        return builder.create();
    }
}
