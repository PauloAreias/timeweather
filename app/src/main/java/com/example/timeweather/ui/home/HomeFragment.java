package com.example.timeweather.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.timeweather.AlarmActivity;
import com.example.timeweather.AlarmAdapter;
import com.example.timeweather.MainActivity;
import com.example.timeweather.R;
import com.example.timeweather.database.entity.Alarme;
import com.example.timeweather.viewModel.AlarmeViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private static final int INSERT_ALARM_REQUEST_CODE = 1;
    private AlarmeViewModel alarmeViewModel;
    private RecyclerView recyclerView;
    private AlarmAdapter alarmAdapter;


    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        alarmAdapter = new AlarmAdapter();

        // mostrar os alarmes ----------------------------------------------------------------

        recyclerView = getView().findViewById(R.id.recycle);
        recyclerView.setAdapter(alarmAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        alarmeViewModel = ViewModelProviders.of(getActivity()).get(AlarmeViewModel.class);
        alarmeViewModel.getAlarmes().observe(getActivity(), new Observer<List<Alarme>>() {
            @Override
            public void onChanged(List<Alarme> alarmes) {
                alarmAdapter.setAlarmes(alarmes);
            }
        });

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                0,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                Alarme myAlarme = alarmAdapter.getAlarmePosition(position);
                Toast.makeText(getActivity(),myAlarme.getNome() + " apagado",Toast.LENGTH_SHORT).show();
                alarmeViewModel.delete(myAlarme);


            }
        });
        helper.attachToRecyclerView(recyclerView);


        FloatingActionButton fab = getView().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AlarmActivity.class);
                startActivityForResult(intent,INSERT_ALARM_REQUEST_CODE);
            }
        });

        // -----------------------------------------------------------------------------------
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == INSERT_ALARM_REQUEST_CODE && resultCode == RESULT_OK){
            String string = data.getStringExtra("alarmes");
            Alarme alarme =new Alarme(string);
            alarmeViewModel.insert(alarme);
        }
    }


}