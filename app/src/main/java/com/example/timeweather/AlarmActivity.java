package com.example.timeweather;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AlarmActivity extends AppCompatActivity {

    private TimePicker timePicker;
   // private ToggleButton toggleButton;
    private Button confirm;
    private EditText alarme;
    private Button btn_cancelar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        timePicker = findViewById(R.id.timepicker);
        timePicker.setIs24HourView(true);
        confirm = findViewById(R.id.btn_confirmar);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AlarmActivity.this,"On",Toast.LENGTH_LONG).show();
            }
        });

        alarme = findViewById(R.id.txt_nomealarme);
        confirm = findViewById(R.id.confirmbtn);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                if(TextUtils.isEmpty(alarme.getText())){
                    String s =(timePicker.getHour() +":" + timePicker.getMinute() + "     " + "Alarme Normal");
                    intent.putExtra("alarmes",s);
                    setResult(RESULT_OK,intent);
                } else {
                    String s =(timePicker.getHour() +":" + timePicker.getMinute() + "     " + alarme.getText().toString());
                    intent.putExtra("alarmes",s);
                    setResult(RESULT_OK,intent);
                }
                finish();
            }
        });
        //--------------------------------------BOTAO CANCELAR-------------------------------------------------------------
        btn_cancelar = findViewById(R.id.btn_cancelar);
        btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });
        //-----------------------------------------------------------------------------------------------------------------
        TextView txt_atr = findViewById(R.id.txt_atraso);
        txt_atr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FragmentDialogAlarmeAtraso().show(getSupportFragmentManager(), "fragmentDialog");
            }
        });
        TextView txt_rep = findViewById(R.id.txt_repetir);
        txt_rep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FragmentDialogAlarmeRepetir().show(getSupportFragmentManager(), "fragmentDialog");
            }
        });
    }
}
