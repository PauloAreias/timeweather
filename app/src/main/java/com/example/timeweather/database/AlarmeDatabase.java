package com.example.timeweather.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.timeweather.database.dao.AlarmeDao;
import com.example.timeweather.database.entity.Alarme;

@Database(entities = Alarme.class,version = 1,  exportSchema = false)
public abstract class AlarmeDatabase extends RoomDatabase {

    private static AlarmeDatabase INSTANCE;
    public abstract AlarmeDao alarmeDao();

    public static AlarmeDatabase getInstance(final Context context){
        if(INSTANCE == null){
            synchronized (AlarmeDatabase.class){
                if(INSTANCE==null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext()
                            ,AlarmeDatabase.class,"alarme.db").build();
                }
            }
        }
        return INSTANCE;
    }

}
