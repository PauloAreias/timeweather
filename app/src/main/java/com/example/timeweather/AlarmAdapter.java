package com.example.timeweather;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.timeweather.database.entity.Alarme;
import com.example.timeweather.ui.home.HomeViewModel;
import com.example.timeweather.viewModel.AlarmeViewModel;

import java.util.List;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder> {

    private Context context;
    private List<Alarme> alarmes;

    public AlarmAdapter(){
        this.context = context;
        this.alarmes = alarmes;
    }

    public void setAlarmes(List<Alarme> aAlarmes) {
        alarmes = aAlarmes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item,parent,false);

        return new AlarmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder holder, int position) {

        if (alarmes != null) {
            Alarme alarme = alarmes.get(position);
            final TextView textView = holder.textView;
            textView.setText(alarme.getNome());
        }
    }



    @Override
    public int getItemCount() {
        if(alarmes!=null){
            return alarmes.size();
        }else {
            return 0;
        }
    }

    public class AlarmViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;

        public AlarmViewHolder(@NonNull View itemView) {
            super(itemView);
            textView= itemView.findViewById(R.id.alarme_view);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context,"Ola",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public Alarme getAlarmePosition(int position){
        return alarmes.get(position);
    }
}
