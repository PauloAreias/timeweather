package com.example.timeweather;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class FragmentDialogAlarmeAtraso extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final String [] lista_atraso = getActivity().getResources().getStringArray(R.array.lista_atraso);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Selecione uma opção:");
        builder.setNeutralButton("Cancelar", null);
        builder.setItems(lista_atraso, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                Toast.makeText(getActivity(), "Escolheu: "+lista_atraso[i], Toast.LENGTH_SHORT).show();
            }
        });

        return builder.create();
    }
}
